Feature: Displays temperature sensor
    Read form I2C bus via XT Pico

    Scenario: Continuously display monitored temperature of related sensor
      Given that a new value is being broadcasted by related EPICS PV
      when LEBT Chopper OPI is receiving updated value from EPICS PV
      Then LEBT Chopper OPI displays a single scalar value for read temperature
