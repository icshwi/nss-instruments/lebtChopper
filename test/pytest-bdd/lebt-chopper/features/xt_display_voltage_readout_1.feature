Feature: Displays voltage readout 1
    Input voltage A (5 Volts) read via XT Pico

    Scenario: Continuously display monitored voltage readout of sensor 1
      Given that a new value is being broadcasted by related EPICS PV
      when LEBT Chopper OPI is receiving updated value from EPICS PV
      Then LEBT Chopper OPI displays a single scalar value for read voltage
