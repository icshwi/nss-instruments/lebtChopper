Feature: Plot signal of PicoScope input channel 1
    Timing signal (EVR)

    Scenario: Plot signals in a single plot for all 4 channels
      Given user launches CS-Studio Phoebus
      When user LEBT Chopper OPI is executed
      Then waveform data for input channel 1 of PicoScope device is showed as one more curve in a common plot chart
