Feature: Change settings of curve of PicoScope input channel 3
    FBIS Input 2

    Scenario: Change time scale of curve plot of input channel 3
      Given user entries a new time scale for the plot
      When user LEBT Chopper OPI is displaying the curve
      Then plotted time scale reflects user:s choice

    Scenario: Change trigger parameters of input channel 3
      Given user entries new trigger parameters for the channel
      When user LEBT Chopper OPI is displaying the curve
      Then plotted channel curve updates according to user:s choice
