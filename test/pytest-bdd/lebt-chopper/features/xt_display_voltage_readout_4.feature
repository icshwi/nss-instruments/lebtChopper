Feature: Displays voltage readout 4
    Input HV switch voltage monitor (5 Volts) read via XT Pico

    Scenario: Continuously display monitored voltage readout of sensor 4
      Given that a new value is being broadcasted by related EPICS PV
      when LEBT Chopper OPI is receiving updated value from EPICS PV
      Then LEBT Chopper OPI displays a single scalar value for read voltage
