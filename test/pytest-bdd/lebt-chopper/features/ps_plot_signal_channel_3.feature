Feature: Plot signal of PicoScope input channel 3
    FBIS Input 2

    Scenario: Plot signals in a single plot for all 4 channels
      Given user launches CS-Studio Phoebus
      When user LEBT Chopper OPI is executed
      Then waveform data for input channel 3 of PicoScope device is showed as one more curve in a common plot chart
