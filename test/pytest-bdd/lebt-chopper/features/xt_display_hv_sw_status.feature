Feature: Displays HV software status
    Read Digital Input at P02 via XT Pico

    Scenario: Continuously display monitored status of HV SW fault input
      Given that a new value is being broadcasted by related EPICS PV
      when LEBT Chopper OPI is receiving updated value from EPICS PV
      Then LEBT Chopper OPI displays a Green LED if PV value is HIGH (1) or a Red LED if PV value is LOW (0)
