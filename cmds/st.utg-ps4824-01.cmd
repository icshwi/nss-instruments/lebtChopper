require(ADCore, 3.9.0)
require(admisc, 7505d306)
require(asyn, 4.40.0)
require(busy, 1.7.2_x)
require(adps4000a, master)


# 
#- avoid messages 'callbackRequest: cbLow ring buffer full'
callbackSetQueueSize(10000)

#- 10 MB max CA request
epicsEnvSet("LOCATION",                     "UTG")
epicsEnvSet("DEVICE_NAME",                  "PS4824-01")
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",     "10000000")
epicsEnvSet("PREFIX",                       "$(LOCATION):$(DEVICE_NAME):")
epicsEnvSet("PORT",                         "PICO")
# epicsEnvSet("NUM_SAMPLES",                  "1000")
epicsEnvSet("MAX_SAMPLES",                  "100000")
epicsEnvSet("XSIZE",                        "$(MAX_SAMPLES)")
epicsEnvSet("YSIZE",                        "1")
epicsEnvSet("QSIZE",                        "20")
epicsEnvSet("NCHANS",                       "100")
epicsEnvSet("CBUFFS",                       "500")
epicsEnvSet("MAX_THREADS",                  "4")

#- create a PicoScope 4000A driver
#- PS4000AConfig(const char *portName, int numSamples, int dataType,
#-               int maxBuffers, int maxMemory, int priority, int stackSize)
#- dataType == NDInt32 == 4
PS4000AConfig("$(PORT)", $(MAX_SAMPLES), 4, 0, 0)

# asynSetTraceIOMask("$(PORT)",0,2)
# asynSetTraceMask("$(PORT)",0,255)

dbLoadRecords("ps4000a.template","P=$(PREFIX),R=,PORT=$(PORT),ADDR=0,TIMEOUT=1,MAX_SAMPLES=$(MAX_SAMPLES)")

#- individual input channels
iocshLoad("$(adps4000a_DIR)channel.iocsh", "ADDR=0, NAME=A")
iocshLoad("$(adps4000a_DIR)channel.iocsh", "ADDR=1, NAME=B")
iocshLoad("$(adps4000a_DIR)channel.iocsh", "ADDR=2, NAME=C")
iocshLoad("$(adps4000a_DIR)channel.iocsh", "ADDR=3, NAME=D")
iocshLoad("$(adps4000a_DIR)channel.iocsh", "ADDR=4, NAME=E")
iocshLoad("$(adps4000a_DIR)channel.iocsh", "ADDR=5, NAME=F")
iocshLoad("$(adps4000a_DIR)channel.iocsh", "ADDR=6, NAME=G")
iocshLoad("$(adps4000a_DIR)channel.iocsh", "ADDR=7, NAME=H")

iocInit()

# -----------------------------------------------------------------------------
# Set some default configurations
# -----------------------------------------------------------------------------
# Enables AD ArrayCallbacks for PicoScope device
dbpf $(PREFIX)ArrayCallbacks 1
# Enables channels A-D:s Callbacks then
dbpf $(PREFIX)A-Enabled 1
dbpf $(PREFIX)B-Enabled 1
dbpf $(PREFIX)C-Enabled 1
dbpf $(PREFIX)D-Enabled 1

