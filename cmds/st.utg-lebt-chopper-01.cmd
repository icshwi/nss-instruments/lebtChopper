
# -----------------------------------------------------------------------------
# LEBT Chopper system - Test Bench
# -----------------------------------------------------------------------------
# Devices being controlled:
#   - XT Pico 
#   - PicoScope 4824
# -----------------------------------------------------------------------------
require(xtpico,0.9.0)
require(ADCore, 3.9.0)
require(admisc, 7505d306)
require(asyn, 4.40.0)
require(busy, 1.7.2_x)
require(adps4000a, master)

#
#- avoid messages 'callbackRequest: cbLow ring buffer full'
callbackSetQueueSize(10000)

# -----------------------------------------------------------------------------
# ---------------------------------- XT Pico ----------------------------------
# -----------------------------------------------------------------------------
# Prefix for all records
epicsEnvSet("PREFIX_XT",        "UTG-XTPICO-01")
epicsEnvSet("DEVICE_IP",        "192.168.100.100")

epicsEnvSet("I2C_COMM_PORT",    "AK_I2C_COMM")
epicsEnvSet("I2C_TMP100_PORT",  "AK_I2C_TMP100")
epicsEnvSet("I2C_LTC2991_PORT", "AK_I2C_LTC2991")
epicsEnvSet("I2C_ADT7420_PORT", "AK_I2C_ADT7420")

# -----------------------------------------------------------------------------
#- Create the asyn port to talk XTpico server on TCP port 1002.
drvAsynIPPortConfigure($(I2C_COMM_PORT),"$(DEVICE_IP):1002")

# -----------------------------------------------------------------------------
# Debug
#- asynSetTraceIOMask("$(I2C_COMM_PORT)",0,255)
#- asynSetTraceMask("$(I2C_COMM_PORT)",0,255)

# -----------------------------------------------------------------------------
# PMT temperature sensor
# -----------------------------------------------------------------------------
#iocshLoad("$(xtpico_DIR)/tmp100.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=1, NAME=:I2C1:Temp1:, COUNT=1, INFOS=0x49")
#AKI2CTMP100Configure("TMP100.1", "AK_I2C_COMM", 1, "0x49", 1, 0, 0)
#dbLoadRecords("AKI2C_TMP100.db", "P=UTG-XTPICO-01, R=:I2C1:Temp1:, PORT=TMP100.1, IP_PORT=AK_I2C_COMM, ADDR=0, TIMEOUT=1")
AKI2CADT7420Configure($(I2C_ADT7420_PORT), $(I2C_COMM_PORT), 1, "0x49", 1, 0, 0)
dbLoadRecords("AKI2C_ADT7420.db", "P=$(PREFIX_XT),R=:I2C1:Temp1:,PORT=$(I2C_ADT7420_PORT),IP_PORT=$(I2C_COMM_PORT),ADDR=0,TIMEOUT=1")

# -----------------------------------------------------------------------------
# Voltage and current monitor
# -----------------------------------------------------------------------------
#iocshLoad("$(xtpico_DIR)/ltc2991.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=2, NAME=:I2C1:VMon1:, COUNT=1, INFOS=0x90")
AKI2CLTC2991Configure($(I2C_LTC2991_PORT), $(I2C_COMM_PORT), 1, "0x48", 0, 0, 1, 0, 0)
dbLoadRecords("AKI2C_LTC2991.db", "P=UTG-XTPICO-01,R=:I2C1:VMon1:,PORT=$(I2C_LTC2991_PORT),IP_PORT=$(I2C_COMM_PORT),ADDR=0,TIMEOUT=1")

# -----------------------------------------------------------------------------
# --------------------------------- PicoScope ---------------------------------
# -----------------------------------------------------------------------------
#- 10 MB max CA request
epicsEnvSet("LOCATION",                     "UTG")
epicsEnvSet("DEVICE_NAME",                  "PS4824-01")
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",     "10000000")
epicsEnvSet("PREFIX_PS",                    "$(LOCATION):$(DEVICE_NAME):")
epicsEnvSet("PORT",                         "PICO")
# epicsEnvSet("NUM_SAMPLES",                  "1000")
epicsEnvSet("MAX_SAMPLES",                  "100000")
epicsEnvSet("XSIZE",                        "$(MAX_SAMPLES)")
epicsEnvSet("YSIZE",                        "1")
epicsEnvSet("QSIZE",                        "20")
epicsEnvSet("NCHANS",                       "100")
epicsEnvSet("CBUFFS",                       "500")
epicsEnvSet("MAX_THREADS",                  "4")

#- create a PicoScope 4000A driver
#- PS4000AConfig(const char *portName, int numSamples, int dataType,
#-               int maxBuffers, int maxMemory, int priority, int stackSize)
#- dataType == NDInt32 == 4
PS4000AConfig("$(PORT)", $(MAX_SAMPLES), 4, 0, 0)

# asynSetTraceIOMask("$(PORT)",0,2)
# asynSetTraceMask("$(PORT)",0,255)

dbLoadRecords("ps4000a.template","P=$(PREFIX_PS),R=,PORT=$(PORT),ADDR=0,TIMEOUT=1,MAX_SAMPLES=$(MAX_SAMPLES)")

#- individual input channels
iocshLoad("$(adps4000a_DIR)channel.iocsh", "ADDR=0, NAME=A")
iocshLoad("$(adps4000a_DIR)channel.iocsh", "ADDR=1, NAME=B")
iocshLoad("$(adps4000a_DIR)channel.iocsh", "ADDR=2, NAME=C")
iocshLoad("$(adps4000a_DIR)channel.iocsh", "ADDR=3, NAME=D")
iocshLoad("$(adps4000a_DIR)channel.iocsh", "ADDR=4, NAME=E")
iocshLoad("$(adps4000a_DIR)channel.iocsh", "ADDR=5, NAME=F")
iocshLoad("$(adps4000a_DIR)channel.iocsh", "ADDR=6, NAME=G")
iocshLoad("$(adps4000a_DIR)channel.iocsh", "ADDR=7, NAME=H")

iocInit()

# -----------------------------------------------------------------------------
# Set some default configurations
# -----------------------------------------------------------------------------
# Enables AD ArrayCallbacks for PicoScope device
dbpf $(PREFIX_PS)ArrayCallbacks 1
# Enables channels A-D:s Callbacks then
dbpf $(PREFIX_PS)A-Enabled 1
dbpf $(PREFIX_PS)B-Enabled 1
dbpf $(PREFIX_PS)C-Enabled 1
dbpf $(PREFIX_PS)D-Enabled 1

